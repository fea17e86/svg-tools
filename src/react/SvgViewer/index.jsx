import PropTypes from "prop-types";
import React from "react";
import SvgInline from "react-svg-inline";

import StyleSelector from "../components/StyleSelector";

function getSvgInlineStyle(style = {}) {
  return {
    ...style,
    overflow: "hidden",
    textAlign: "center"
  };
}

const defaultStyles = [
  {
    name: "Pattern",
    style: {
      background:
        "linear-gradient(45deg, rgba(0,0,0,0.0980392) 25%, rgba(0,0,0,0) 25%, rgba(0,0,0,0) 75%, rgba(0,0,0,0.0980392) 75%, rgba(0,0,0,0.0980392) 0), linear-gradient(45deg, rgba(0,0,0,0.0980392) 25%, rgba(0,0,0,0) 25%, rgba(0,0,0,0) 75%, rgba(0,0,0,0.0980392) 75%, rgba(0,0,0,0.0980392) 0), rgb(255, 255, 255)",
      backgroundPosition: "0 0, 10px 10px",
      backgroundOrigin: "padding-box",
      backgroundClip: "border-box",
      backgroundSize: "20px 20px"
    }
  },
  {
    name: "Red",
    style: { backgroundColor: "red" }
  },
  {
    name: "White",
    style: { backgroundColor: "white" }
  },
  {
    name: "Dark",
    style: { backgroundColor: "#444444" }
  },
  {
    name: "Black",
    style: { backgroundColor: "black" }
  }
];

function renderStyleSelector({ onChange, selectedStyle, ...props }) {
  return (
    <StyleSelector {...props} selected={selectedStyle} onChange={onChange} />
  );
}

function renderSvg({ style, ...props }) {
  return (
    <SvgInline {...props} style={getSvgInlineStyle(style)} component="div" />
  );
}

export default class SvgViewer extends React.Component {
  static propTypes = {
    svg: PropTypes.string.isRequired,
    render: PropTypes.func
  };

  state = {
    selectedStyle: defaultStyles[0]
  };

  selectStyle = selectedStyle => {
    this.setState({ selectedStyle });
  };

  render() {
    const { render, svg, ...props } = this.props;
    const { selectedStyle } = this.state;

    return render
      ? render({
          renderSvg: additionalProps =>
            renderSvg({
              ...props,
              svg,
              style: selectedStyle.style,
              ...additionalProps
            }),
          renderStyleSelector: additionalProps =>
            renderStyleSelector({
              ...props,
              selectedStyle,
              onChange: this.selectStyle,
              styles: defaultStyles,
              ...additionalProps
            })
        })
      : renderSvg({ ...props, style: selectedStyle.style });
  }
}
