import {
  Navbar,
  NavbarBurger,
  NavbarDropdown,
  NavbarItem,
  NavbarLink,
  NavbarMenu,
  NavbarStart
} from "bloomer";

import PropTypes from "prop-types";
import React from "react";

export default class Nav extends React.Component {
  static propTypes = {
    onOpenFile: PropTypes.func,
    onSaveFile: PropTypes.func,
    style: PropTypes.object
  };

  static defaultProps = {
    style: undefined
  };

  state = {
    isMenuOpen: false
  };

  toggleMenuOpen = () => {
    this.setState(({ isMenuOpen }) => ({
      isMenuOpen: !isMenuOpen
    }));
  };

  render() {
    return (
      <Navbar className="is-dark" style={this.props.style}>
        <NavbarBurger
          isActive={this.state.isMenuOpen}
          onClick={this.toggleMenuOpen}
        />
        <NavbarMenu
          isActive={this.state.isMenuOpen}
          onClick={this.toggleMenuOpen}
        >
          <NavbarStart>
            <NavbarItem hasDropdown isHoverable>
              <NavbarLink href="#">File</NavbarLink>
              <NavbarDropdown>
                <NavbarItem href="#" onClick={this.props.onOpenFile}>
                  Open
                </NavbarItem>
                <NavbarItem href="#" onClick={this.props.onSaveFile}>
                  Save
                </NavbarItem>
              </NavbarDropdown>
            </NavbarItem>
          </NavbarStart>
        </NavbarMenu>
      </Navbar>
    );
  }
}
