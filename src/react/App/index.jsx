import {
  Button,
  Column,
  Columns,
  Container,
  Delete,
  Icon,
  Modal,
  ModalBackground,
  ModalCard,
  ModalCardBody,
  ModalCardFooter,
  ModalCardHeader,
  ModalCardTitle
} from "bloomer";
import React from "react";
import ReactDOM from "react-dom";
import { Value } from "react-powerplug";

import Nav from "./Nav";
import CodeTextArea from "../components/CodeTextArea";
import Tool from "../components/Tool";
import SvgEditor from "../SvgEditor";
import SvgVariables from "../SvgVariables";
import SvgViewer from "../SvgViewer";

import Head from "../../svgs/Head";

function fillTemplate(templateString, templateVars) {
  try {
    // eslint-disable-next-line
    return new Function("return `" + templateString + "`;").call(templateVars);
  } catch (e) {}
  return templateString;
}

function normalizeState(state = {}) {
  const { svg, variables } = state;
  return { svg, variables };
}

export class App extends React.Component {
  state = {
    isOpeningFile: false,
    isSavingFile: false,
    svg: Head,
    variables: [
      {
        name: "color1",
        type: "color",
        value: "#FFFFFF"
      }
    ]
  };

  getSvgVariables() {
    return this.state.variables.reduce(
      (variables, { name, value }) => ({
        ...variables,
        [name]: value
      }),
      {}
    );
  }

  getSvg() {
    return fillTemplate(this.state.svg, this.getSvgVariables());
  }

  closeFileModal = () => {
    this.setState({ isOpeningFile: false, isSavingFile: false });
  };

  openFile = () => {
    this.setState({ isOpeningFile: true });
  };

  saveFile = () => {
    this.setState({ isSavingFile: true });
  };

  setApplicationState = (state = {}) => {
    try {
      this.setState(
        normalizeState(
          typeof state === "object" ? state : JSON.parse(`${state}`)
        )
      );
    } catch (e) {
      console.error("setApplicationState", e);
    }
  };

  setSvg = svg => {
    this.setState({ svg });
  };

  addVariable = () => {
    this.setState(({ variables }) => {
      const changedVariables = [].concat(variables);
      changedVariables.push({
        name: `variable${variables.length}`,
        type: "text",
        value: ""
      });
      return { variables: changedVariables };
    });
  };

  changeVariable = ({ index, ...props }) => {
    this.setState(({ variables }) => {
      const changedVariables = [].concat(variables);
      changedVariables[index] = props;
      return { variables: changedVariables };
    });
  };

  deleteVariable = index => {
    this.setState(({ variables }) => ({
      variables: variables.slice(0, index).concat(variables.slice(index + 1))
    }));
  };

  renderNav() {
    return (
      <Nav
        onOpenFile={this.openFile}
        onSaveFile={this.saveFile}
        style={{ marginBottom: "1.5rem" }}
      />
    );
  }

  renderVariables() {
    return (
      <Tool>
        <span title="true">Variables</span>
        <span actions="true">
          <Button
            actions="true"
            isColor="success"
            onClick={this.addVariable}
            title="Add a variable"
          >
            <Icon className="fa fa-plus" />
          </Button>
        </span>
        <SvgVariables
          variables={this.state.variables}
          onAdd={this.addVariable}
          onChange={this.changeVariable}
          onDelete={this.deleteVariable}
        />
      </Tool>
    );
  }

  renderEditor() {
    return (
      <Tool>
        <span title="true">Editor</span>
        <SvgEditor svg={this.state.svg} onChange={this.setSvg} />
      </Tool>
    );
  }

  renderViewer() {
    return (
      <SvgViewer
        svg={this.getSvg()}
        render={({ renderSvg, renderStyleSelector }) => {
          return (
            <Tool>
              <span title="true">Viewer</span>
              {[
                renderSvg({ key: "svg" }),
                renderStyleSelector({
                  actions: "true",
                  key: "style-selector"
                })
              ]}
            </Tool>
          );
        }}
      />
    );
  }

  renderFileModal() {
    const { isOpeningFile, isSavingFile } = this.state;

    return (
      <Value>
        {({ value, setValue }) => (
          <Modal isActive={isOpeningFile || isSavingFile}>
            <ModalBackground />
            <ModalCard>
              <ModalCardHeader>
                <ModalCardTitle>
                  {isOpeningFile
                    ? "Paste application state"
                    : isSavingFile
                      ? "Copy application state"
                      : ""}
                </ModalCardTitle>
                <Delete onClick={this.closeFileModal} />
              </ModalCardHeader>
              <ModalCardBody>
                <CodeTextArea
                  autoHeight
                  value={
                    isSavingFile
                      ? JSON.stringify(normalizeState(this.state), null, 2)
                      : value
                  }
                  onChange={setValue}
                />
              </ModalCardBody>
              {isOpeningFile && (
                <ModalCardFooter>
                  <Button
                    isColor="success"
                    onClick={() => {
                      this.setApplicationState(value);
                      this.closeFileModal();
                    }}
                  >
                    Open
                  </Button>
                </ModalCardFooter>
              )}
            </ModalCard>
          </Modal>
        )}
      </Value>
    );
  }

  render() {
    return (
      <Container isFluid>
        {this.renderNav()}
        <Columns>
          <Column>{this.renderVariables()}</Column>
        </Columns>
        <Columns>
          <Column>{this.renderEditor()}</Column>
          <Column isSize="narrow">{this.renderViewer()}</Column>
        </Columns>
        {this.renderFileModal()}
      </Container>
    );
  }
}

export default function mountApp() {
  ReactDOM.render(<App />, document.getElementById("root"));
}
