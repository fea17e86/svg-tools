import { TextArea } from "bloomer";
import PropTypes from "prop-types";
import React from "react";

function getStyle(style = {}) {
  return {
    ...style,
    background: "#ffffff",
    fontFamily: "monospace",
    fontSize: "0.875em",
    whiteSpace: "pre"
  };
}

CodeTextArea.propTypes = {
  autoHeight: PropTypes.bool,
  onChange: PropTypes.func,
  style: PropTypes.object,
  value: PropTypes.string
};

export default function CodeTextArea({
  autoHeight = false,
  onChange = () => {},
  style,
  value = ""
}) {
  const props = {
    value,
    onChange: ({ target: { value } }) => onChange(value),
    style: getStyle(style)
  };

  if (autoHeight) {
    props.rows = value.split("\n").length;
  }

  return <TextArea {...props} />;
}
