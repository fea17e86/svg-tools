import { Control, Icon, Select } from "bloomer";
import PropTypes from "prop-types";
import React from "react";

function getStyleIconStyle(style = {}) {
  return {
    ...style,
    display: "block",
    padding: "0.25em",
    height: "100%",
    width: "100%"
  };
}

StyleSelector.proptypes = {
  onChange: PropTypes.func,
  styles: PropTypes.arrayOf(PropTypes.object),
  selected: PropTypes.object
};

export default function StyleSelector({
  onChange = () => {},
  selected = {},
  styles = [],
  ...props
}) {
  return (
    <Control hasIcons="left" {...props}>
      <Select
        defaultValue={selected.name}
        onChange={({ target: { value } }) => {
          onChange(styles.find(({ name }) => name === value));
        }}
      >
        {styles.map(({ name, style }) => (
          <option key={name} value={name}>
            {name}
          </option>
        ))}
      </Select>
      <Icon
        isSize="small"
        isAlign="left"
        style={{ padding: "calc(.375em - 1px)" }}
      >
        <span style={getStyleIconStyle(selected.style)} />
      </Icon>
    </Control>
  );
}
