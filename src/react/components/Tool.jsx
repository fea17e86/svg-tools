import { Card, CardContent, CardHeader, CardHeaderTitle } from "bloomer";
import PropTypes from "prop-types";
import React from "react";
import seapig, { OPTIONAL, OPTIONALS } from "seapig";

function getCardHeaderActionsStyle(style = {}) {
  return {
    alignItem: "center",
    display: "flex",
    flexGrow: 0,
    padding: "0.74rem",
    ...style
  };
}

CardHeaderActions.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object
};

function CardHeaderActions({ children, style, ...props }) {
  return (
    <div {...props} style={getCardHeaderActionsStyle(style)}>
      {children}
    </div>
  );
}

function renderHeader(title, actions) {
  if (actions || title) {
    return (
      <CardHeader>
        {title && <CardHeaderTitle>{title}</CardHeaderTitle>}
        {actions && <CardHeaderActions>{actions}</CardHeaderActions>}
      </CardHeader>
    );
  }
}

function renderContent(content) {
  if (content) {
    return <CardContent>{content}</CardContent>;
  }
}

export default function Tool({ children }) {
  const { actionsChildren, titleChildren, rest } = seapig(children, {
    actions: OPTIONALS,
    title: OPTIONAL
  });

  return (
    <Card>
      {renderHeader(titleChildren, actionsChildren)}
      {renderContent(rest)}
    </Card>
  );
}
