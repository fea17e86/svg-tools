import {
  Button,
  Column,
  Columns,
  Control,
  Field,
  Icon,
  Input,
  Select
} from "bloomer";
import PropTypes from "prop-types";
import React from "react";

const inputStyle = {
  width: 100
};

const variableTypes = ["color", "number", "text"];

SvgVariable.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  onChange: PropTypes.func,
  onDelete: PropTypes.func
};

export function SvgVariable({
  name,
  onChange = () => {},
  onDelete,
  type,
  value
}) {
  return (
    <Field hasAddons>
      <Control>
        <Select defaultValue={type} isColor="primary" title="type">
          {variableTypes.map(variableType => (
            <option key={variableType} value={variableType}>
              {variableType}
            </option>
          ))}
        </Select>
      </Control>
      <Control>
        <Input
          isColor="primary"
          placeholder="name"
          onChange={({ target: { value: name } }) =>
            onChange({ name, type, value })
          }
          style={inputStyle}
          title="name"
          type="text"
          value={name}
        />
      </Control>
      <Control>
        <Input
          isColor="primary"
          placeholder="value"
          onChange={({ target: { value } }) => onChange({ name, type, value })}
          style={inputStyle}
          title="value"
          type={type}
          value={value}
        />
      </Control>
      <Control>
        <Button isColor="danger" onClick={onDelete} title="Delete varaible">
          <Icon className="fa fa-trash" />
        </Button>
      </Control>
    </Field>
  );
}

SvgVariables.propTypes = {
  variables: PropTypes.array.isRequired,
  onAdd: PropTypes.func,
  onChange: PropTypes.func,
  onDelete: PropTypes.func
};

export default function SvgVariables({
  onAdd = () => {},
  onDelete = () => {},
  onChange = () => {},
  variables
}) {
  return (
    <Columns>
      {variables.map(({ name, ...props }, index) => (
        <Column isSize="narrow" key={`variable-${index}`}>
          <SvgVariable
            {...props}
            name={name}
            onChange={variable => onChange({ ...variable, index })}
            onDelete={() => onDelete(index)}
          />
        </Column>
      ))}
    </Columns>
  );
}
