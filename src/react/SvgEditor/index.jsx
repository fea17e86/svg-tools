import PropTypes from "prop-types";
import React from "react";

import CodeTextArea from "../components/CodeTextArea";

function renderCodeEditor(props) {
  return <CodeTextArea autoHeight {...props} />;
}

export default class SvgEditor extends React.Component {
  static propTypes = {
    svg: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    render: PropTypes.func
  };

  render() {
    const { onChange, render, svg, ...rest } = this.props;

    const props = {
      ...rest,
      onChange,
      value: svg
    };

    return render
      ? render({
          render: (additionalProps = {}) =>
            renderCodeEditor({ ...props, ...additionalProps })
        })
      : renderCodeEditor(props);
  }
}
