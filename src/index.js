import mountApp from "./react/App";
import registerServiceWorker from "./registerServiceWorker";

import "../node_modules/bulma/css/bulma.min.css";

mountApp();
registerServiceWorker();
